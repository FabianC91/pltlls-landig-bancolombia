$(function() {
	var swiper = new Swiper('.swiper-container-featured', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
         function autoHeightObjt(select){
        if($(window).width()>=990)//Is a desktop
        {
            var objHeight=[];
            var maxHeight = 0;
            $(select).each(function(i, item){
                objHeight.push($(item));
                if(maxHeight<objHeight[i].height())
                maxHeight = objHeight[i].height();
            });
            for (i = 0; i < objHeight.length; ++i) {
                objHeight[i].height(maxHeight);
            }
        }
    }
    autoHeightObjt(".featured-father");
});