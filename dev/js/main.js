$(function() {
	// Swiper Config
    var swiper = new Swiper('.swiper-contenedor', {
        pagination: '.swiper-pagination-2',
        paginationClickable: true
    });

    var swiper = new Swiper('.swiper-container-featured', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });

    var swiper = new Swiper('.swiper-container-need', {
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 36,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    var swiper = new Swiper('.swiper-container-icon', {
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 30,
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    var md = new MobileDetect(window.navigator.userAgent);
	if(md.phone()){
		$('.info-slide h5').hide();
		$('.featured-content .small a').hide();
		$('#need-p').hide();
        $('.services-content hr').show();
        $('#menu-top').hide();
		$('#breadcrumbs').hide();
	}

    var $fiImage = $('#featured .featured-content img');
    objectFitImages($fiImage);


    function autoHeightObjt(select){
    	if($(window).width()>=990)//Is a desktop
    	{
	        var objHeight=[];
	        var maxHeight = 0;
        	$(select).each(function(i, item){
            	objHeight.push($(item));
            	if(maxHeight<objHeight[i].height())
            	maxHeight = objHeight[i].height();
    		});
        	for (i = 0; i < objHeight.length; ++i) {
            	objHeight[i].height(maxHeight);
        	}
    	}
	}
	autoHeightObjt(".featured-father");

    $('#slider').siblings("p").css("margin", "0" );
});
