$(function() {
	var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        autoplay: 10000,
        paginationClickable: true
    });
});