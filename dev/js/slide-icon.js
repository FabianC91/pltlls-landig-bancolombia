$(function() {
	var swiper = new Swiper('.swiper-container-icon', {
		slidesPerView: 3,
		paginationClickable: true,
		spaceBetween: 30,
		breakpoints: {
			1024: {
				slidesPerView: 3,
				spaceBetween: 30
			},
			768: {
				slidesPerView: 3,
				spaceBetween: 30
			},
			640: {
				slidesPerView: 1,
				spaceBetween: 20
			},
			320: {
				slidesPerView: 1,
				spaceBetween: 10
			}
		}
	});
});