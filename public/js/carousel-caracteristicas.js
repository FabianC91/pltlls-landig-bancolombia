$('.responsive').slick({
dots: true,
infinite: true,
speed: 300,
slidesToShow: 3,
slidesToScroll: 1,
nextArrow: '<img class="nextSliderProject nextSliderProjectLeft btn-caro btn-caro-next" src="/wps/wcm/connect/c3c5aa89-6f8b-464a-b4fc-25a5a7e2144b/next.png?MOD=AJPERES">',
prevArrow: '<img class="nextSliderProject nextSliderProjectRight btn-caro btn-caro-prev" src="/wps/wcm/connect/7c5c5b9d-fb89-4087-a6bf-0e5465e09c08/prev.png?MOD=AJPERES">',
responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }

]
});
