(function() {
    'use strict';

    document.querySelectorAll('input').forEach(x => {
        x.onfocus = function() {
            x.parentNode.className = 'small';
        }

        x.onblur = function() {
            if (x.value.trim() == "") {
                x.parentNode.className = '';
            }
        }
    });
})();