
$(document).ready(function(){

        $('.slick-invct').slick({
                dots: true,

                infinite: true,
              
                // speed: 300,
              
                autoplay: false,
              
                autoplaySpeed: 5000,
                adaptiveHeight: true,
                slidesToShow: 1,
              
                slidesToScroll: 1,
              
                responsive: [
              
                  {
              
                    breakpoint: 1024,
              
                    settings: {
              
                      slidesToShow: 1,
              
                      slidesToScroll: 1,
              
                      infinite: true,
              
                      dots: false
              
                    }
              
                  },
              
                  {
              
                    breakpoint: 600,
              
                    settings: {
              
                      slidesToShow: 1,
              
                      slidesToScroll: 1
              
                    }
              
                  },
              
                  {
              
                    breakpoint: 480,
              
                    settings: {
              
                      slidesToShow: 1,
              
                      slidesToScroll: 1
              
                    }
              
                  }
              
                ]
         } );

         //Tab Mobile
         $("#nav-tabs-mobile_txt").on("click", function () {
              $(".nav-tabs-mobile ul").toggle();
         });
         $(".nav-tabs-mobile a").on("click", function () {
           var text = $(this).text();
           $("#nav-tabs-mobile_txt").text(text);
          $("#nav-tabs-mobile_txt").trigger("click");
         })
});