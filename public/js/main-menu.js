/* Menú general oculto scroll */
$(window).scroll(function() {
	if ($(this).scrollTop()>300) {
		$('#header-small').slideUp(); 
		// console.log("me metí en el primer bloque");
	} else {
		$('#header-small').slideDown();
		// console.log("me metí en el segundo bloque");
		//$('.menuMobile .menu').animate({"margin-top": "30px"});  
	}
});
$(window).scroll(function() {
	if($('#header-small').css('display') == 'none') {
		$('.menu').addClass("intro"); 
	} else {
		$('.menu').removeClass("intro"); 
	}
});

(function() {
		var menuEl = document.getElementById('ml-menu'),
			mlmenu = new MLMenu(menuEl, {
				// breadcrumbsCtrl : true, // show breadcrumbs
				// initialBreadcrumb : 'all', // initial breadcrumb text
				backCtrl : false, // show back button
				// itemsDelayInterval : 60, // delay between each menu item sliding animation
				onItemClick: loadDummyData // callback: item that doesn´t have a submenu gets clicked - onItemClick([event], [inner HTML of the clicked item])
			});

		// mobile menu toggle
		var openMenuCtrl = document.querySelector('.action--open'),
			closeMenuCtrl = document.querySelector('.action--close');

		openMenuCtrl.addEventListener('click', openMenu);
		closeMenuCtrl.addEventListener('click', closeMenu);

		function openMenu() {
			classie.add(menuEl, 'menu--open');
		}

		function closeMenu() {
			classie.remove(menuEl, 'menu--open');
		}

		// simulate grid content loading
		var gridWrapper = document.querySelector('.content');

		function loadDummyData(ev, itemName) {
			ev.preventDefault();

			closeMenu();
			gridWrapper.innerHTML = '';
			classie.add(gridWrapper, 'content--loading');
			setTimeout(function() {
				classie.remove(gridWrapper, 'content--loading');
				gridWrapper.innerHTML = '<ul class="products">' + dummyData[itemName] + '<ul>';
			}, 700);
		}

		$( "#item-sub" ).click(function() {
  			$( "#item-sub > ul" ).toggle( "slow" );
		});

		$( "#transacciones" ).click(function() {
  			$( ".buttonHeader .menu-transacciones" ).toggle( "slow" );
		});

	})();



