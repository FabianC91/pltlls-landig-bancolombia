//btn Menu
$(document).ready(function() {
    if($(window).width()<=768){
        $('#transacciones a').css('height', '64px' );
        $('#transacciones a').css('padding', '20px 6px 10px 29px');
        $('#transacciones a').css('background-position', '11px 24px');
    }
});

// Tablas responsive
$(document).ready(function() {
  $('.table-responsive').each(function(){
    $(this).basictable({
    breakpoint: 768
    });
  });
});

/* BUSCADOR HOME */
$(document).ready(function() {
    if($('.buscador-home').length>0) {
        var alto = $(window).height()-$('.buscador-home').offset().top;
        if (alto-$('.buscador-home .buscador').height()>$('.buscador-home').height()) {
            $('.buscador-home .fondo').height($(window).height()-$('.buscador-home .fondo').offset().top);
            $('.buscador-home').css({'min-height': $('.fondo').height()});
        } else {
            $('.buscador-home').css({'min-height': $('.buscador-home').height()+100+$('.buscador-home .buscador').height()});
        }
    }
});

/*AUTOPAUTA */
$(document).ready(function() {
    if ($('.home-autopauta-derecha').length>0) {
        $('.home-autopauta-derecha').height($('.home-autopauta-derecha .contenido').height()+$('.home-autopauta-derecha .titulo').height());
        $(window).resize(function() {
            $('.home-autopauta-derecha').height($('.home-autopauta-derecha .contenido').height()+$('.home-autopauta-derecha .titulo').height());
        })
    }
    if ($('.home-doble-autopauta-derecha').length>0) {
        $('.home-doble-autopauta-derecha').height($('.home-doble-autopauta-derecha .contenido').height()+$('.home-doble-autopauta-derecha .pauta').height());
        $('.home-doble-autopauta-derecha .fondo').height($('.home-doble-autopauta-derecha').height());
        $(window).resize(function() {
            $('.home-doble-autopauta-derecha').height($('.home-doble-autopauta-derecha .contenido').height()+$('.home-doble-autopauta-derecha .pauta').height());
        })
    }
    if ($('.home-entradilla-con-bkg-img').length>0) {
        $('.home-entradilla-con-bkg-img').height($('.home-entradilla-con-bkg-img .contenido').height()+$('.home-entradilla-con-bkg-img .pauta').height());
        $('.home-entradilla-con-bkg-img .fondo').height($('.home-entradilla-con-bkg-img').height());
        $(window).resize(function() {
            $('.home-entradilla-con-bkg-img').height($('.home-entradilla-con-bkg-img .contenido').height()+$('.home-entradilla-con-bkg-img .pauta').height());
        })
    }
});

/* Wizard */
myApp.controller('Wizard', ['$scope', '$log', '$http', '$timeout', function($scope, $log, $http, $timeout) {
    $scope.wizard = wizard;
    $scope.seccion = 1;
    $scope.selector = null;
    $scope.showFilter = function(selector) {
        $scope.seccion = 3;
        $scope.selector = selector;
    }
}]);


/* Wizard */
myApp.controller('WizardController', ['$scope', '$log', '$http', '$timeout', function($scope, $log, $http, $timeout) {

    $scope.showSection = function(indice) {
        $scope.actualWizard = indice;
        $timeout(function() {
            $('html, body').animate({
                scrollTop: $("#wizard"+indice).offset().top
            }, 1000);
        }, 10);
    }
}]);

/* SEARCHBAR */
$(document).ready(function() {
    $( "#btn_search" ).click(function() {
        $(".topsearchbar").slideToggle("slow");
        $( "#btn_search" ).toggleClass("btn-close");
    });
});

/* Tabs Horizontales */
$(document).ready(function() {
    $('.tabs-horizontales ul.nav-tabs > li > a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    //$($('.nav-tabs')[0]).affix({offset:{top:$($('.nav-tabs')[0]).offset().top}});
    /*$('.tabs-horizontales ul.nav-tabs').each(function(index, element) {
        $(element).affix({offset:{top:$(element).offset().top}});
    });*/
})


/* NavegaciÃƒÂ³n Vertical */

$(document).ready(function() {
    $('.tabs-verticales .nav-tabs > li > a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    responsiveTabs.init();
})

var responsiveTabs = ( function ( $, responsiveTabs ) {

    responsiveTabs.init = function () {

        responsiveTabs.currentPosition = 'tabs';

        var tabGroups = $( '.nav-tabs.responsive' );
        var hidden    = '';
        var visible   = '';
        var activeTab = '';

        collapseDisplayed = [ 'xs', 'sm' ];

        $.each( collapseDisplayed, function () {
            hidden  += ' hidden-' + this;
            visible += ' visible-' + this;
        } );

        $.each(tabGroups, function () {
            var $tabGroup   = $( this );
            var tabs        = $tabGroup.find( 'li a' );
            var collapseDiv = $( '<div></div>', {
                'class' : 'acordeon ' + visible,
                'id'    : 'collapse-' + (parseInt(Math.random()*100))
            } );

            $.each( tabs, function () {
                var $this          = $( this );
                var oldLinkClass   = $this.attr( 'class' ) === undefined ? '' : $this.attr( 'class' );
                var newLinkClass   = 'accordion-toggle';
                var oldParentClass = $this.parent().attr( 'class' ) === undefined ? '' : $this.parent().attr( 'class' );
                var newParentClass = 'panel panel-default';
                var newHash        = $this.get( 0 ).hash.replace( '#', 'collapse-' );

                if ( oldLinkClass.length > 0 ) {
                    newLinkClass += ' ' + oldLinkClass;
                }

                if ( oldParentClass.length > 0 ) {
                    oldParentClass = oldParentClass.replace( /\bactive\b/g, '' );
                    newParentClass += ' ' + oldParentClass;
                    newParentClass = newParentClass.replace( /\s{2,}/g, ' ' );
                    newParentClass = newParentClass.replace( /^\s+|\s+$/g, '' );
                }

                if ( $this.parent().hasClass( 'active' ) ) {
                    activeTab = '#' + newHash;
                }
                var titulo = '<h3 class="titulo_acordeon">'+$this.html()+'<li class="pull-right glyphicon glyphicon-plus"></li></h3>'
                collapseDiv.append(titulo);
                collapseDiv.append('<div class="pane"><div class="row"><div id="'+newHash+'" class="col-xs-12">'+$($(this).parent().find('a').attr('href')).html()+'</div></div></div>');
            } );

            $tabGroup.next().after( collapseDiv );
            $tabGroup.addClass( hidden );
            $( '.tab-content.responsive' ).addClass( hidden );
        } );

        if ( activeTab ) {
            $( activeTab ).collapse( 'show' );
        }
    };

    return responsiveTabs;
}( window.jQuery, responsiveTabs || { } ) );

/*Acordeon*/
$(document).ready(function() {
    $(".acordeon > h3.titulo_acordeon").click(function() {
        clearCurrentAccordion($(this).parent());
        var alturaTitulo = $(this).outerHeight();

        if (!$(this).hasClass("current") && $(this).next().queue().length === 0) {
            $.when($(this).next().slideDown()).then(function(){
                $("html, body").animate({scrollTop: $(this).offset().top - alturaTitulo }, 400);
            });
            $(this).addClass("current");
            $(this).find('li').removeClass('glyphicon-plus').addClass('glyphicon-minus');
            $(this).find('span').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
    });

    function clearCurrentAccordion(accordion)
    {
        $(accordion).find("h3.titulo_acordeon").each(function(i, item){
            $(this).next().slideUp();
            $(item).removeClass("current");
            $(this).find('li').addClass('glyphicon-plus').removeClass('glyphicon-minus');
            $(this).find('span').addClass('glyphicon-plus').removeClass('glyphicon-minus');
        });
    }

    /* Habilitando el primero por defecto
    $('.acordeon').each(function() {
        $($(this).find('h3.titulo_acordeon')[0]).trigger('click');
    });*/
});


/* Paginador */

$(document).ready(function() {
    $('.paginador').each(function() {
        var active = $(this).find('.active').text();
        var total = $(this).find('.pagina:last-child').text();
        var mensaje = 'Pagina '+active+' de '+total;
        $(this).find('.pagina-xs').html(mensaje);
        $(this).find('.paginas > .hidden-xs');
        var paginas = $($(this).find('.paginas > .hidden-xs')[0]);
        var ancho = 100/paginas.children().length;
        //console.log(ancho);
        $(paginas.children()).each(function() {
            $(this).css({width:ancho+"%"});
        });
    });

});

/* Ayuda */
$(document).ready(function() {
    var ayuda = $('a[rel="ayuda"]');
    var currentAyuda = null;

    ayuda.click(function(evt) {
        evt.preventDefault();
        closeAyuda();
        currentAyuda = $($(evt.currentTarget).attr('href'));
        currentAyuda.css({"margin-top": - currentAyuda.height()/2});
        currentAyuda.animate({"left":'0'});
    });

    $('.ayuda-contextual .close').click(function(evt) {
        evt.preventDefault();
        closeAyuda();
    });

    function closeAyuda() {
        if (currentAyuda) {
            currentAyuda.animate({"left":'-100%'});
            currentAyuda = null;
        }
    }
});

function GetScreenCordinates(obj) {
    var p = {};
    p.x = obj.offsetLeft;
    p.y = obj.offsetTop;
    while (obj.offsetParent) {
        p.x = p.x + obj.offsetParent.offsetLeft;
        p.y = p.y + obj.offsetParent.offsetTop;
        if (obj == document.getElementsByTagName("body")[0]) {
            break;
        }
        else {
            obj = obj.offsetParent;
        }
    }
    return p;
}

/* GalerÃƒÂ­a */
!function($){$.bcslider=function(e,t){var a=$(e);a.vars=$.extend({},$.bcslider.defaults,t);var n=a.vars.namespace,i=window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture,s=("ontouchstart"in window||i||window.DocumentTouch&&document instanceof DocumentTouch)&&a.vars.touch,r="click touchend MSPointerUp keyup",o="",l,c="vertical"===a.vars.direction,d=a.vars.reverse,u=a.vars.itemWidth>0,v="fade"===a.vars.animation,p=""!==a.vars.asNavFor,m={},f=!0;$.data(e,"bcslider",a),m={init:function(){a.animating=!1,a.currentSlide=parseInt(a.vars.startAt?a.vars.startAt:0,10),isNaN(a.currentSlide)&&(a.currentSlide=0),a.animatingTo=a.currentSlide,a.atEnd=0===a.currentSlide||a.currentSlide===a.last,a.containerSelector=a.vars.selector.substr(0,a.vars.selector.search(" ")),a.slides=$(a.vars.selector,a),a.container=$(a.containerSelector,a),a.count=a.slides.length,a.syncExists=$(a.vars.sync).length>0,"slide"===a.vars.animation&&(a.vars.animation="swing"),a.prop=c?"top":"marginLeft",a.args={},a.manualPause=!1,a.stopped=!1,a.started=!1,a.startTimeout=null,a.transitions=!a.vars.video&&!v&&a.vars.useCSS&&function(){var e=document.createElement("div"),t=["perspectiveProperty","WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var n in t)if(void 0!==e.style[t[n]])return a.pfx=t[n].replace("Perspective","").toLowerCase(),a.prop="-"+a.pfx+"-transform",!0;return!1}(),a.ensureAnimationEnd="",""!==a.vars.controlsContainer&&(a.controlsContainer=$(a.vars.controlsContainer).length>0&&$(a.vars.controlsContainer)),""!==a.vars.manualControls&&(a.manualControls=$(a.vars.manualControls).length>0&&$(a.vars.manualControls)),""!==a.vars.customDirectionNav&&(a.customDirectionNav=2===$(a.vars.customDirectionNav).length&&$(a.vars.customDirectionNav)),a.vars.randomize&&(a.slides.sort(function(){return Math.round(Math.random())-.5}),a.container.empty().append(a.slides)),a.doMath(),a.setup("init"),a.vars.controlNav&&m.controlNav.setup(),a.vars.directionNav&&m.directionNav.setup(),a.vars.keyboard&&(1===$(a.containerSelector).length||a.vars.multipleKeyboard)&&$(document).bind("keyup",function(e){var t=e.keyCode;if(!a.animating&&(39===t||37===t)){var n=39===t?a.getTarget("next"):37===t?a.getTarget("prev"):!1;a.flexAnimate(n,a.vars.pauseOnAction)}}),a.vars.mousewheel&&a.bind("mousewheel",function(e,t,n,i){e.preventDefault();var s=a.getTarget(0>t?"next":"prev");a.flexAnimate(s,a.vars.pauseOnAction)}),a.vars.pausePlay&&m.pausePlay.setup(),a.vars.slideshow&&a.vars.pauseInvisible&&m.pauseInvisible.init(),a.vars.slideshow&&(a.vars.pauseOnHover&&a.hover(function(){a.manualPlay||a.manualPause||a.pause()},function(){a.manualPause||a.manualPlay||a.stopped||a.play()}),a.vars.pauseInvisible&&m.pauseInvisible.isHidden()||(a.vars.initDelay>0?a.startTimeout=setTimeout(a.play,a.vars.initDelay):a.play())),p&&m.asNav.setup(),s&&a.vars.touch&&m.touch(),(!v||v&&a.vars.smoothHeight)&&$(window).bind("resize orientationchange focus",m.resize),a.find("img").attr("draggable","false"),setTimeout(function(){a.vars.start(a)},200)},asNav:{setup:function(){a.asNav=!0,a.animatingTo=Math.floor(a.currentSlide/a.move),a.currentItem=a.currentSlide,a.slides.removeClass(n+"active-slide").eq(a.currentItem).addClass(n+"active-slide"),i?(e._slider=a,a.slides.each(function(){var e=this;e._gesture=new MSGesture,e._gesture.target=e,e.addEventListener("MSPointerDown",function(e){e.preventDefault(),e.currentTarget._gesture&&e.currentTarget._gesture.addPointer(e.pointerId)},!1),e.addEventListener("MSGestureTap",function(e){e.preventDefault();var t=$(this),n=t.index();$(a.vars.asNavFor).data("bcslider").animating||t.hasClass("active")||(a.direction=a.currentItem<n?"next":"prev",a.flexAnimate(n,a.vars.pauseOnAction,!1,!0,!0))})})):a.slides.on(r,function(e){e.preventDefault();var t=$(this),i=t.index(),s=t.offset().left-$(a).scrollLeft();0>=s&&t.hasClass(n+"active-slide")?a.flexAnimate(a.getTarget("prev"),!0):$(a.vars.asNavFor).data("bcslider").animating||t.hasClass(n+"active-slide")||(a.direction=a.currentItem<i?"next":"prev",a.flexAnimate(i,a.vars.pauseOnAction,!1,!0,!0))})}},controlNav:{setup:function(){a.manualControls?m.controlNav.setupManual():m.controlNav.setupPaging()},setupPaging:function(){var e="thumbnails"===a.vars.controlNav?"control-thumbs":"control-paging",t=1,i,s;if(a.controlNavScaffold=$('<ol class="'+n+"control-nav "+n+e+'"></ol>'),a.pagingCount>1)for(var l=0;l<a.pagingCount;l++){if(s=a.slides.eq(l),i="thumbnails"===a.vars.controlNav?'<img src="'+s.attr("data-thumb")+'"/>':"<a>"+t+"</a>","thumbnails"===a.vars.controlNav&&!0===a.vars.thumbCaptions){var c=s.attr("data-thumbcaption");""!==c&&void 0!==c&&(i+='<span class="'+n+'caption">'+c+"</span>")}a.controlNavScaffold.append("<li>"+i+"</li>"),t++}a.controlsContainer?$(a.controlsContainer).append(a.controlNavScaffold):a.append(a.controlNavScaffold),m.controlNav.set(),m.controlNav.active(),a.controlNavScaffold.delegate("a, img",r,function(e){if(e.preventDefault(),""===o||o===e.type){var t=$(this),i=a.controlNav.index(t);t.hasClass(n+"active")||(a.direction=i>a.currentSlide?"next":"prev",a.flexAnimate(i,a.vars.pauseOnAction))}""===o&&(o=e.type),m.setToClearWatchedEvent()})},setupManual:function(){a.controlNav=a.manualControls,m.controlNav.active(),a.controlNav.bind(r,function(e){if(e.preventDefault(),""===o||o===e.type){var t=$(this),i=a.controlNav.index(t);t.hasClass(n+"active")||(a.direction=i>a.currentSlide?"next":"prev",a.flexAnimate(i,a.vars.pauseOnAction))}""===o&&(o=e.type),m.setToClearWatchedEvent()})},set:function(){var e="thumbnails"===a.vars.controlNav?"img":"a";a.controlNav=$("."+n+"control-nav li "+e,a.controlsContainer?a.controlsContainer:a)},active:function(){a.controlNav.removeClass(n+"active").eq(a.animatingTo).addClass(n+"active")},update:function(e,t){a.pagingCount>1&&"add"===e?a.controlNavScaffold.append($("<li><a>"+a.count+"</a></li>")):1===a.pagingCount?a.controlNavScaffold.find("li").remove():a.controlNav.eq(t).closest("li").remove(),m.controlNav.set(),a.pagingCount>1&&a.pagingCount!==a.controlNav.length?a.update(t,e):m.controlNav.active()}},directionNav:{setup:function(){var e=$('<ul class="'+n+'direction-nav"><li class="'+n+'nav-prev"><a class="'+n+'prev" href="#">'+a.vars.prevText+'</a></li><li class="'+n+'nav-next"><a class="'+n+'next" href="#">'+a.vars.nextText+"</a></li></ul>");a.customDirectionNav?a.directionNav=a.customDirectionNav:a.controlsContainer?($(a.controlsContainer).append(e),a.directionNav=$("."+n+"direction-nav li a",a.controlsContainer)):(a.append(e),a.directionNav=$("."+n+"direction-nav li a",a)),m.directionNav.update(),a.directionNav.bind(r,function(e){e.preventDefault();var t;(""===o||o===e.type)&&(t=a.getTarget($(this).hasClass(n+"next")?"next":"prev"),a.flexAnimate(t,a.vars.pauseOnAction)),""===o&&(o=e.type),m.setToClearWatchedEvent()})},update:function(){var e=n+"disabled";1===a.pagingCount?a.directionNav.addClass(e).attr("tabindex","-1"):a.vars.animationLoop?a.directionNav.removeClass(e).removeAttr("tabindex"):0===a.animatingTo?a.directionNav.removeClass(e).filter("."+n+"prev").addClass(e).attr("tabindex","-1"):a.animatingTo===a.last?a.directionNav.removeClass(e).filter("."+n+"next").addClass(e).attr("tabindex","-1"):a.directionNav.removeClass(e).removeAttr("tabindex")}},pausePlay:{setup:function(){var e=$('<div class="'+n+'pauseplay"><a></a></div>');a.controlsContainer?(a.controlsContainer.append(e),a.pausePlay=$("."+n+"pauseplay a",a.controlsContainer)):(a.append(e),a.pausePlay=$("."+n+"pauseplay a",a)),m.pausePlay.update(a.vars.slideshow?n+"pause":n+"play"),a.pausePlay.bind(r,function(e){e.preventDefault(),(""===o||o===e.type)&&($(this).hasClass(n+"pause")?(a.manualPause=!0,a.manualPlay=!1,a.pause()):(a.manualPause=!1,a.manualPlay=!0,a.play())),""===o&&(o=e.type),m.setToClearWatchedEvent()})},update:function(e){"play"===e?a.pausePlay.removeClass(n+"pause").addClass(n+"play").html(a.vars.playText):a.pausePlay.removeClass(n+"play").addClass(n+"pause").html(a.vars.pauseText)}},touch:function(){function t(t){t.stopPropagation(),a.animating?t.preventDefault():(a.pause(),e._gesture.addPointer(t.pointerId),w=0,p=c?a.h:a.w,f=Number(new Date),l=u&&d&&a.animatingTo===a.last?0:u&&d?a.limit-(a.itemW+a.vars.itemMargin)*a.move*a.animatingTo:u&&a.currentSlide===a.last?a.limit:u?(a.itemW+a.vars.itemMargin)*a.move*a.currentSlide:d?(a.last-a.currentSlide+a.cloneOffset)*p:(a.currentSlide+a.cloneOffset)*p)}function n(t){t.stopPropagation();var a=t.target._slider;if(a){var n=-t.translationX,i=-t.translationY;return w+=c?i:n,m=w,y=c?Math.abs(w)<Math.abs(-n):Math.abs(w)<Math.abs(-i),t.detail===t.MSGESTURE_FLAG_INERTIA?void setImmediate(function(){e._gesture.stop()}):void((!y||Number(new Date)-f>500)&&(t.preventDefault(),!v&&a.transitions&&(a.vars.animationLoop||(m=w/(0===a.currentSlide&&0>w||a.currentSlide===a.last&&w>0?Math.abs(w)/p+2:1)),a.setProps(l+m,"setTouch"))))}}function s(e){e.stopPropagation();var t=e.target._slider;if(t){if(t.animatingTo===t.currentSlide&&!y&&null!==m){var a=d?-m:m,n=t.getTarget(a>0?"next":"prev");t.canAdvance(n)&&(Number(new Date)-f<550&&Math.abs(a)>50||Math.abs(a)>p/2)?t.flexAnimate(n,t.vars.pauseOnAction):v||t.flexAnimate(t.currentSlide,t.vars.pauseOnAction,!0)}r=null,o=null,m=null,l=null,w=0}}var r,o,l,p,m,f,g,h,S,y=!1,x=0,b=0,w=0;i?(e.style.msTouchAction="none",e._gesture=new MSGesture,e._gesture.target=e,e.addEventListener("MSPointerDown",t,!1),e._slider=a,e.addEventListener("MSGestureChange",n,!1),e.addEventListener("MSGestureEnd",s,!1)):(g=function(t){a.animating?t.preventDefault():(window.navigator.msPointerEnabled||1===t.touches.length)&&(a.pause(),p=c?a.h:a.w,f=Number(new Date),x=t.touches[0].pageX,b=t.touches[0].pageY,l=u&&d&&a.animatingTo===a.last?0:u&&d?a.limit-(a.itemW+a.vars.itemMargin)*a.move*a.animatingTo:u&&a.currentSlide===a.last?a.limit:u?(a.itemW+a.vars.itemMargin)*a.move*a.currentSlide:d?(a.last-a.currentSlide+a.cloneOffset)*p:(a.currentSlide+a.cloneOffset)*p,r=c?b:x,o=c?x:b,e.addEventListener("touchmove",h,!1),e.addEventListener("touchend",S,!1))},h=function(e){x=e.touches[0].pageX,b=e.touches[0].pageY,m=c?r-b:r-x,y=c?Math.abs(m)<Math.abs(x-o):Math.abs(m)<Math.abs(b-o);var t=500;(!y||Number(new Date)-f>t)&&(e.preventDefault(),!v&&a.transitions&&(a.vars.animationLoop||(m/=0===a.currentSlide&&0>m||a.currentSlide===a.last&&m>0?Math.abs(m)/p+2:1),a.setProps(l+m,"setTouch")))},S=function(t){if(e.removeEventListener("touchmove",h,!1),a.animatingTo===a.currentSlide&&!y&&null!==m){var n=d?-m:m,i=a.getTarget(n>0?"next":"prev");a.canAdvance(i)&&(Number(new Date)-f<550&&Math.abs(n)>50||Math.abs(n)>p/2)?a.flexAnimate(i,a.vars.pauseOnAction):v||a.flexAnimate(a.currentSlide,a.vars.pauseOnAction,!0)}e.removeEventListener("touchend",S,!1),r=null,o=null,m=null,l=null},e.addEventListener("touchstart",g,!1))},resize:function(){!a.animating&&a.is(":visible")&&(u||a.doMath(),v?m.smoothHeight():u?(a.slides.width(a.computedW),a.update(a.pagingCount),a.setProps()):c?(a.viewport.height(a.h),a.setProps(a.h,"setTotal")):(a.vars.smoothHeight&&m.smoothHeight(),a.newSlides.width(a.computedW),a.setProps(a.computedW,"setTotal")))},smoothHeight:function(e){if(!c||v){var t=v?a:a.viewport;e?t.animate({height:a.slides.eq(a.animatingTo).height()},e):t.height(a.slides.eq(a.animatingTo).height())}},sync:function(e){var t=$(a.vars.sync).data("bcslider"),n=a.animatingTo;switch(e){case"animate":t.flexAnimate(n,a.vars.pauseOnAction,!1,!0);break;case"play":t.playing||t.asNav||t.play();break;case"pause":t.pause()}},uniqueID:function(e){return e.filter("[id]").add(e.find("[id]")).each(function(){var e=$(this);e.attr("id",e.attr("id")+"_clone")}),e},pauseInvisible:{visProp:null,init:function(){var e=m.pauseInvisible.getHiddenProp();if(e){var t=e.replace(/[H|h]idden/,"")+"visibilitychange";document.addEventListener(t,function(){m.pauseInvisible.isHidden()?a.startTimeout?clearTimeout(a.startTimeout):a.pause():a.started?a.play():a.vars.initDelay>0?setTimeout(a.play,a.vars.initDelay):a.play()})}},isHidden:function(){var e=m.pauseInvisible.getHiddenProp();return e?document[e]:!1},getHiddenProp:function(){var e=["webkit","moz","ms","o"];if("hidden"in document)return"hidden";for(var t=0;t<e.length;t++)if(e[t]+"Hidden"in document)return e[t]+"Hidden";return null}},setToClearWatchedEvent:function(){clearTimeout(l),l=setTimeout(function(){o=""},3e3)}},a.flexAnimate=function(e,t,i,r,o){if(a.vars.animationLoop||e===a.currentSlide||(a.direction=e>a.currentSlide?"next":"prev"),p&&1===a.pagingCount&&(a.direction=a.currentItem<e?"next":"prev"),!a.animating&&(a.canAdvance(e,o)||i)&&a.is(":visible")){if(p&&r){var l=$(a.vars.asNavFor).data("bcslider");if(a.atEnd=0===e||e===a.count-1,l.flexAnimate(e,!0,!1,!0,o),a.direction=a.currentItem<e?"next":"prev",l.direction=a.direction,Math.ceil((e+1)/a.visible)-1===a.currentSlide||0===e)return a.currentItem=e,a.slides.removeClass(n+"active-slide").eq(e).addClass(n+"active-slide"),!1;a.currentItem=e,a.slides.removeClass(n+"active-slide").eq(e).addClass(n+"active-slide"),e=Math.floor(e/a.visible)}if(a.animating=!0,a.animatingTo=e,t&&a.pause(),a.vars.before(a),a.syncExists&&!o&&m.sync("animate"),a.vars.controlNav&&m.controlNav.active(),u||a.slides.removeClass(n+"active-slide").eq(e).addClass(n+"active-slide"),a.atEnd=0===e||e===a.last,a.vars.directionNav&&m.directionNav.update(),e===a.last&&(a.vars.end(a),a.vars.animationLoop||a.pause()),v)s?(a.slides.eq(a.currentSlide).css({opacity:0,zIndex:1}),a.slides.eq(e).css({opacity:1,zIndex:2}),a.wrapup(f)):(a.slides.eq(a.currentSlide).css({zIndex:1}).animate({opacity:0},a.vars.animationSpeed,a.vars.easing),a.slides.eq(e).css({zIndex:2}).animate({opacity:1},a.vars.animationSpeed,a.vars.easing,a.wrapup));else{var f=c?a.slides.filter(":first").height():a.computedW,g,h,S;u?(g=a.vars.itemMargin,S=(a.itemW+g)*a.move*a.animatingTo,h=S>a.limit&&1!==a.visible?a.limit:S):h=0===a.currentSlide&&e===a.count-1&&a.vars.animationLoop&&"next"!==a.direction?d?(a.count+a.cloneOffset)*f:0:a.currentSlide===a.last&&0===e&&a.vars.animationLoop&&"prev"!==a.direction?d?0:(a.count+1)*f:d?(a.count-1-e+a.cloneOffset)*f:(e+a.cloneOffset)*f,a.setProps(h,"",a.vars.animationSpeed),a.transitions?(a.vars.animationLoop&&a.atEnd||(a.animating=!1,a.currentSlide=a.animatingTo),a.container.unbind("webkitTransitionEnd transitionend"),a.container.bind("webkitTransitionEnd transitionend",function(){clearTimeout(a.ensureAnimationEnd),a.wrapup(f)}),clearTimeout(a.ensureAnimationEnd),a.ensureAnimationEnd=setTimeout(function(){a.wrapup(f)},a.vars.animationSpeed+100)):a.container.animate(a.args,a.vars.animationSpeed,a.vars.easing,function(){a.wrapup(f)})}a.vars.smoothHeight&&m.smoothHeight(a.vars.animationSpeed)}},a.wrapup=function(e){v||u||(0===a.currentSlide&&a.animatingTo===a.last&&a.vars.animationLoop?a.setProps(e,"jumpEnd"):a.currentSlide===a.last&&0===a.animatingTo&&a.vars.animationLoop&&a.setProps(e,"jumpStart")),a.animating=!1,a.currentSlide=a.animatingTo,a.vars.after(a)},a.animateSlides=function(){!a.animating&&f&&a.flexAnimate(a.getTarget("next"))},a.pause=function(){clearInterval(a.animatedSlides),a.animatedSlides=null,a.playing=!1,a.vars.pausePlay&&m.pausePlay.update("play"),a.syncExists&&m.sync("pause")},a.play=function(){a.playing&&clearInterval(a.animatedSlides),a.animatedSlides=a.animatedSlides||setInterval(a.animateSlides,a.vars.slideshowSpeed),a.started=a.playing=!0,a.vars.pausePlay&&m.pausePlay.update("pause"),a.syncExists&&m.sync("play")},a.stop=function(){a.pause(),a.stopped=!0},a.canAdvance=function(e,t){var n=p?a.pagingCount-1:a.last;return t?!0:p&&a.currentItem===a.count-1&&0===e&&"prev"===a.direction?!0:p&&0===a.currentItem&&e===a.pagingCount-1&&"next"!==a.direction?!1:e!==a.currentSlide||p?a.vars.animationLoop?!0:a.atEnd&&0===a.currentSlide&&e===n&&"next"!==a.direction?!1:a.atEnd&&a.currentSlide===n&&0===e&&"next"===a.direction?!1:!0:!1},a.getTarget=function(e){return a.direction=e,"next"===e?a.currentSlide===a.last?0:a.currentSlide+1:0===a.currentSlide?a.last:a.currentSlide-1},a.setProps=function(e,t,n){var i=function(){var n=e?e:(a.itemW+a.vars.itemMargin)*a.move*a.animatingTo,i=function(){if(u)return"setTouch"===t?e:d&&a.animatingTo===a.last?0:d?a.limit-(a.itemW+a.vars.itemMargin)*a.move*a.animatingTo:a.animatingTo===a.last?a.limit:n;switch(t){case"setTotal":return d?(a.count-1-a.currentSlide+a.cloneOffset)*e:(a.currentSlide+a.cloneOffset)*e;case"setTouch":return d?e:e;case"jumpEnd":return d?e:a.count*e;case"jumpStart":return d?a.count*e:e;default:return e}}();return-1*i+"px"}();a.transitions&&(i=c?"translate3d(0,"+i+",0)":"translate3d("+i+",0,0)",n=void 0!==n?n/1e3+"s":"0s",a.container.css("-"+a.pfx+"-transition-duration",n),a.container.css("transition-duration",n)),a.args[a.prop]=i,(a.transitions||void 0===n)&&a.container.css(a.args),a.container.css("transform",i)},a.setup=function(e){if(v)a.slides.css({width:"100%","float":"left",marginRight:"-100%",position:"relative"}),"init"===e&&(s?a.slides.css({opacity:0,display:"block",webkitTransition:"opacity "+a.vars.animationSpeed/1e3+"s ease",zIndex:1}).eq(a.currentSlide).css({opacity:1,zIndex:2}):0==a.vars.fadeFirstSlide?a.slides.css({opacity:0,display:"block",zIndex:1}).eq(a.currentSlide).css({zIndex:2}).css({opacity:1}):a.slides.css({opacity:0,display:"block",zIndex:1}).eq(a.currentSlide).css({zIndex:2}).animate({opacity:1},a.vars.animationSpeed,a.vars.easing)),a.vars.smoothHeight&&m.smoothHeight();else{var t,i;"init"===e&&(a.viewport=$('<div class="'+n+'viewport"></div>').css({overflow:"hidden",position:"relative"}).appendTo(a).append(a.container),a.cloneCount=0,a.cloneOffset=0,d&&(i=$.makeArray(a.slides).reverse(),a.slides=$(i),a.container.empty().append(a.slides))),a.vars.animationLoop&&!u&&(a.cloneCount=2,a.cloneOffset=1,"init"!==e&&a.container.find(".clone").remove(),a.container.append(m.uniqueID(a.slides.first().clone().addClass("clone")).attr("aria-hidden","true")).prepend(m.uniqueID(a.slides.last().clone().addClass("clone")).attr("aria-hidden","true"))),a.newSlides=$(a.vars.selector,a),t=d?a.count-1-a.currentSlide+a.cloneOffset:a.currentSlide+a.cloneOffset,c&&!u?(a.container.height(200*(a.count+a.cloneCount)+"%").css("position","absolute").width("100%"),setTimeout(function(){a.newSlides.css({display:"block"}),a.doMath(),a.viewport.height(a.h),a.setProps(t*a.h,"init")},"init"===e?100:0)):(a.container.width(200*(a.count+a.cloneCount)+"%"),a.setProps(t*a.computedW,"init"),setTimeout(function(){a.doMath(),a.newSlides.css({width:a.computedW,"float":"left",display:"block"}),a.vars.smoothHeight&&m.smoothHeight()},"init"===e?100:0))}u||a.slides.removeClass(n+"active-slide").eq(a.currentSlide).addClass(n+"active-slide"),a.vars.init(a)},a.doMath=function(){var e=a.slides.first(),t=a.vars.itemMargin,n=a.vars.minItems,i=a.vars.maxItems;a.w=void 0===a.viewport?a.width():a.viewport.width(),a.h=e.height(),a.boxPadding=e.outerWidth()-e.width(),u?(a.itemT=a.vars.itemWidth+t,a.minW=n?n*a.itemT:a.w,a.maxW=i?i*a.itemT-t:a.w,a.itemW=a.minW>a.w?(a.w-t*(n-1))/n:a.maxW<a.w?(a.w-t*(i-1))/i:a.vars.itemWidth>a.w?a.w:a.vars.itemWidth,a.visible=Math.floor(a.w/a.itemW),a.move=a.vars.move>0&&a.vars.move<a.visible?a.vars.move:a.visible,a.pagingCount=Math.ceil((a.count-a.visible)/a.move+1),a.last=a.pagingCount-1,a.limit=1===a.pagingCount?0:a.vars.itemWidth>a.w?a.itemW*(a.count-1)+t*(a.count-1):(a.itemW+t)*a.count-a.w-t):(a.itemW=a.w,a.pagingCount=a.count,a.last=a.count-1),a.computedW=a.itemW-a.boxPadding},a.update=function(e,t){a.doMath(),u||(e<a.currentSlide?a.currentSlide+=1:e<=a.currentSlide&&0!==e&&(a.currentSlide-=1),a.animatingTo=a.currentSlide),a.vars.controlNav&&!a.manualControls&&("add"===t&&!u||a.pagingCount>a.controlNav.length?m.controlNav.update("add"):("remove"===t&&!u||a.pagingCount<a.controlNav.length)&&(u&&a.currentSlide>a.last&&(a.currentSlide-=1,a.animatingTo-=1),m.controlNav.update("remove",a.last))),a.vars.directionNav&&m.directionNav.update()},a.addSlide=function(e,t){var n=$(e);a.count+=1,a.last=a.count-1,c&&d?void 0!==t?a.slides.eq(a.count-t).after(n):a.container.prepend(n):void 0!==t?a.slides.eq(t).before(n):a.container.append(n),a.update(t,"add"),a.slides=$(a.vars.selector+":not(.clone)",a),a.setup(),a.vars.added(a)},a.removeSlide=function(e){var t=isNaN(e)?a.slides.index($(e)):e;a.count-=1,a.last=a.count-1,isNaN(e)?$(e,a.slides).remove():c&&d?a.slides.eq(a.last).remove():a.slides.eq(e).remove(),a.doMath(),a.update(t,"remove"),a.slides=$(a.vars.selector+":not(.clone)",a),a.setup(),a.vars.removed(a)},m.init()},$(window).blur(function(e){focused=!1}).focus(function(e){focused=!0}),$.bcslider.defaults={namespace:"bc-",selector:".slides > li",animation:"fade",easing:"swing",direction:"horizontal",reverse:!1,animationLoop:!0,smoothHeight:!1,startAt:0,slideshow:!0,slideshowSpeed:7e3,animationSpeed:600,initDelay:0,randomize:!1,fadeFirstSlide:!0,thumbCaptions:!1,pauseOnAction:!0,pauseOnHover:!1,pauseInvisible:!0,useCSS:!0,touch:!0,video:!1,controlNav:!0,directionNav:!0,prevText:"Previous",nextText:"Next",keyboard:!0,multipleKeyboard:!1,mousewheel:!1,pausePlay:!1,pauseText:"Pause",playText:"Play",controlsContainer:"",manualControls:"",customDirectionNav:"",sync:"",asNavFor:"",itemWidth:0,itemMargin:0,minItems:1,maxItems:0,move:0,allowOneSlide:!0,start:function(){},before:function(){},after:function(){},end:function(){},added:function(){},removed:function(){},init:function(){}},$.fn.bcslider=function(e){if(void 0===e&&(e={}),"object"==typeof e)return this.each(function(){var t=$(this),a=e.selector?e.selector:".slides > li",n=t.find(a);1===n.length&&e.allowOneSlide===!0||0===n.length?(n.fadeIn(400),e.start&&e.start(t)):void 0===t.data("bcslider")&&new $.bcslider(this,e)});var t=$(this).data("bcslider");switch(e){case"play":t.play();break;case"pause":t.pause();break;case"stop":t.stop();break;case"next":t.flexAnimate(t.getTarget("next"),!0);break;case"prev":case"previous":t.flexAnimate(t.getTarget("prev"),!0);break;default:"number"==typeof e&&t.flexAnimate(e,!0)}}}(jQuery);
$(window).load(function() {

    var galeria = $('.galeria');
    galeria.each(function(index, element) {
        $(element).addClass('galeria-'+index);
        carousel = $($(element).children('.carousel'));
        slider = $($(element).children('.slider'));
        numero = $($(element).find('.numero'));
        carousel.html(slider.html());
        carousel.bcslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 100,
            itemMargin: 5,
            asNavFor: '.galeria-'+index+' .slider'
        });
        slider.bcslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: '.galeria-'+index+' .carousel',
            before: function(evt) {
                $('.galeria-'+index+' .actual').html((evt.animatingTo+1)+' de ');
            }
        });
        numero.html(($(element).find('ul.slides'))[0].children.length);
    });

});

/*Paso a paso Aprende */
$(document).ready(function() {
    $(".paso-a-paso-aprende:even").css("background-color","#f1f1f2");
    $(".paso-a-paso-aprende:odd").css("background-color","#fff");
});


/* Slide */
var slide = $('.slide .owl-carousel');
if (slide.length>0) {
    $(document).ready(function() {
        $(window).resize(function() {
            try {
                $('.slide .owl-carousel').data('owlCarousel').destroy();
                $('.slide .owl-carousel').html($('.owl-stage-outer').html());
            }catch(e){
            }
            initSlide();
        });
        initSlide();
    });
}

function initSlide() {
    var slide = $('.slide .owl-carousel');
    if (slide) {
        slide.owlCarousel({
            items:1,
            nav: true,
            navText: ["<",">"],
            dots: true,
            loop: true,
            responsive: false,
            autoHeight : true,
        });

        slide.on('changed.owl.carousel', function(event) {
            $('.slide .ribbon').html($($($($(event.target).find('.item')[event.item.index])[0]).find('.tipo')[0]).html());
            $(event.target).animate({'height': $($(event.target).find('.item')[event.item.index])[0].clientHeight});
        });
        slide.on('initialized.owl.carousel', function(event) {
            //console.log("Test");
            //console.log($($($($(event.target).find('.item')[0])[0]).find('.tipo')[0]).html());
            //$('.ribbon').html($($($($(event.target).find('.item')[0])[0]).find('.tipo')[0]).html());
            //$(event.target).animate({'height': $($(event.target).find('.item')[0])[0].clientHeight});
        })
        $('.slide .ribbon').html($(".owl-item.active .tipo").text());
    }
}

function initSlideVideos() {
    var carrousel = $('.videos .owl-carousel');
    carrousel.owlCarousel({
        navText: ["<",">"],
        loop: false,
        responsive: {
            0: {
                items: 1,
                loop: $('.videos .owl-carousel .item').size() > 1 ? true:false,
                nav: $('.videos .owl-carousel .item').size() > 1 ? true:false,
                dots: false
            },
            768: {
                items: 2,
                loop: $('.videos .owl-carousel .item').size() > 3 ? true:false,
                nav: $('.videos .owl-carousel .item').size() > 3 ? true:false,
                dots: false
            },
            992: {
                items: 3,
                loop: $('.videos .owl-carousel .item').size() > 3 ? true:false,
                nav: $('.videos .owl-carousel .item').size() > 3 ? true:false,
                dots: false
            },
        },
        margin: 15
    });
    if ($('.videos .owl-carousel .item').size() < 4){
        $('.cont-videos-carousel').removeClass('cont-videos');
        //$('.owl-carousel').unwrap();
    }
}

initSlideVideos();


var slide_rotate = $('#slide-capital .owl-carousel');
slide_rotate.owlCarousel({
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:6000,
    autoplayHoverPause: true
});


$(document).ready(function(){
   $(window).resize(function() {
      if ($(window).width() <= 767) {
        $('footer .check-list').hide();
        $('.title').on('click', function(e) {
          e.preventDefault();
            $(".title").removeClass('active');
            $(this).addClass('active');
              $(this).nextUntil('.active').slideDown('slow');
              $(".title").not(".active").nextUntil('.title').slideUp('slow');
        });
      }else {
            $('footer .check-list').show();
        };
   });
});


$(document).ready(function() {

    //Ocultar breadcrumb en el home
    //if(location.pathname.search(/^(\/wps\/portal)?\/(personas|empresas)(\/)?$/i)>=0 || location.pathname.search(/^(\/wps\/portal)?\/(personas|empresas)\/\!ut\//i) >=0) $(".breadcrumbs").hide();


    function autoHeightObjt(select){
        if($(window).width()>=768)//Is a desktop
        {

            //$(".tabs-verticales .tab-content > div").addClass("active");    // Mostrar los tab-pane para calcular el alto de los elementos
            var objHeight=[];
            var maxHeight = 0;

            $(select).each(function(i, item){
                objHeight.push($(item));
                if(maxHeight<objHeight[i].height())
                    maxHeight = objHeight[i].height();
            });
            for (i = 0; i < objHeight.length; ++i) {
                objHeight[i].height(maxHeight);
            }
            //$(".tabs-verticales .tab-content > div").removeClass("active"); // Quitar el active de los tab-pane para que se oculten
            //$(".tabs-verticales .tab-content > div:first").addClass("active"); // Mostrar el primero
        }
    }
    if($(".tabs-verticales").length) $(".tabs-horizontal .tab-content").css("min-height", $(".tabs-verticales #filialTabs").height());
    if($(".tabs-verticales").length && $(".tabs-verticales .nav-tabs").length) $(".tabs-horizontal .tab-content").css("min-height", $(".tabs-verticales .nav-tabs").height());
    autoHeightObjt(".col-md-6>.entradilla-con-imagenes .contenido"); //resumen de un mismo tamano
    autoHeightObjt(".col-md-4>.entradilla-con-imagenes .contenido"); //resumen de un mismo tamano
    autoHeightObjt(".col-md-3>.entradilla-con-imagenes .contenido"); //resumen de un mismo tamano
    autoHeightObjt(".entradilla-con-imagenes .contenido"); //resumen de un mismo tamano
    autoHeightObjt(".entradilla-con-imagen-lateral .contenido"); //resumen de un mismo tamano
    autoHeightObjt(".giros.exterior .answer"); //respuestas giros
    autoHeightObjt(".giros.exterior .answer-two"); //respuestas 2 giros
    autoHeightObjt(".destacados-categoria>.row-fluid div:nth-child(2)"); //destacados categoria (sirve para wizard)
    autoHeightObjt(".slide-productos .productos .destacados-categoria>.row-fluid");
    autoHeightObjt(".productos .destacados-categoria>.row-fluid");
    autoHeightObjt(".categoria-relacionada>.row-fluid div:nth-child(2)"); //categoria relacionada
    autoHeightObjt(".necesidad-descubre>.row-fluid div:nth-child(2)"); //destacado de necesidad empresa
    autoHeightObjt(".canales-destacados>.row-fluid div:nth-child(2)"); //canales destacados
    autoHeightObjt(".canales-home h2"); // canales home
    autoHeightObjt(".canales-home ul.check-list"); // canales home
    autoHeightObjt(".entradilla-icono-superior"); //entradilla con icono superior
    autoHeightObjt(".encabezado .btn"); //botones header
    autoHeightObjt(".entradilla-con-fondo .btn"); //entradilla con fondo
    autoHeightObjt(".biografia-voceros"); // voceros
    autoHeightObjt(".biografia-voceros-texto .bio-info"); // voceros
    autoHeightObjt(".biografia-voceros-texto .cargo-vocero"); //cargo voceros
    autoHeightObjt(".biografia-voceros .bio-info"); // voceros variacion
    autoHeightObjt(".biografia-voceros .cargo-vocero"); //cargo voceros variaciÃ³n
    autoHeightObjt(".biografia-voceros .nombre-vocero"); // variaciÃ³n html voceros
    autoHeightObjt(".credencial .contenido");// credenciales
    autoHeightObjt(".credencial h2");// credenciales
    autoHeightObjt(".credencial p");// credenciales
    autoHeightObjt(".credencial .cuadro");// credenciales
    autoHeightObjt(".tarjeta .details");//comparador tarjetas detalles
    autoHeightObjt(".noticia-destacada .contenido");//noticias filiales
    autoHeightObjt(".sucursal > div:first-child");// puntos de atencion
    autoHeightObjt(".establecimiento");//noticias filiales
    autoHeightObjt(".tips-app .col-sm-12");//landing Empresas
    autoHeightObjt(".tips-app ul li strong");//landing Empresas
    autoHeightObjt(".list-conferences p");//charlas

    //wizard - resumenes
    var seccion;
    var actual_wizard;
    $('.destacado-wizard a').click(function () {
     $(".col-md-4 .entradilla-con-imagenes .contenido, .col-md-4 .entradilla-con-imagen-lateral .contenido").removeAttr("style");
     $(".biografia-voceros, .bio-info, .cargo-vocero, .nombre-vocero").removeAttr("style");
        seccion = $(this)[0].getAttribute("ng-click");
        if (seccion == 'showSection(1)') {
            actual_wizard = "#wizard1";
        }else if(seccion == 'showSection(2)'){
            actual_wizard = "#wizard2";
        }else if(seccion == 'showSection(3)'){
            actual_wizard = "#wizard3";
        }else {
            actual_wizard = "#wizard4";
        };
        classToHeight1 = actual_wizard + " .col-md-4 .entradilla-con-imagenes .contenido"
        classToHeight2 = actual_wizard + " .col-md-4 .entradilla-con-imagen-lateral .contenido"
        autoHeightObjt(classToHeight1);
        autoHeightObjt(classToHeight2);
        autoHeightObjt("#none .col-md-4 .entradilla-con-imagenes .contenido");
        autoHeightObjt(".biografia-voceros");
        autoHeightObjt(".bio-info");
        autoHeightObjt(".cargo-vocero");
        autoHeightObjt(".nombre-vocero");
    });

    //voceros dentro de tabs
    $('.tabs-verticales li a').click(function () {
     $(".biografia-voceros, .bio-info, .cargo-vocero, .nombre-vocero").removeAttr("style");
        autoHeightObjt(".biografia-voceros");
        autoHeightObjt(".bio-info");
        autoHeightObjt(".cargo-vocero");
        autoHeightObjt(".nombre-vocero");
    });

    //comparador tarjetas
    $('.ng-binding, .ng-valid, .tarjeta-wizard').click(function() {
        $(".tarjeta .details").removeAttr("style");
        autoHeightObjt(".tarjeta .details");
    });

});

/* Compartir articulo */
$(document).ready(function() {
    var twitterShare = document.querySelector('[data-js="twitter-share"]');
    if(twitterShare) {
        twitterShare.onclick = function(e) {
            e.preventDefault();
            var twitterWindow = window.open('https://twitter.com/share?url=' + document.URL, 'twitter-popup', 'height=350,width=600');
            if(twitterWindow.focus) { twitterWindow.focus(); }
            return false;
        }
    }
    var facebookShare = document.querySelector('[data-js="facebook-share"]');
    if(facebookShare) {
        facebookShare.onclick = function(e) {
            e.preventDefault();
            var facebookWindow = window.open('https://www.facebook.com/sharer/sharer.php?u=' + document.URL, 'facebook-popup', 'height=350,width=600');
            if(facebookWindow.focus) { facebookWindow.focus(); }
            return false;
        }
    }
    var linkedinShare = document.querySelector('[data-js="linkedin-share"]');
    if(linkedinShare) {
        linkedinShare.onclick = function(e) {
            e.preventDefault();
            var linkedinWindow = window.open('https://www.linkedin.com/cws/share?url=' + document.URL, 'linkedin-popup', 'height=350,width=600');
            if(linkedinWindow.focus) { linkedinWindow.focus(); }
            return false;
        }
    }
});

$(document).ready(function() {
    var fondos = $('.encabezado > .fondo');
    var len = fondos.length;
    if(len>0) {
        $(window).resize(function() {
            positionHeader();
        });
        positionHeader();
    }
});

function positionHeader() {
    var fondos = $('.encabezado > .fondo');
    var len = fondos.length;
    for(var i=0;i<len;i++) {
        var elem = $(fondos[i]);
        var newBpx = '50%';
        var bpy = '50%';
        if($(document).width()<992) {
            var bp = elem.css('backgroundPosition').split(' ');
            //newBpx = (parseFloat(bp[0]) + 5) + 'px'
            newBpx = "right"
            bpy = bp[1];
            if($(document).width()<560) {
                newBpx = ($(document).width()-560)/2;
                var porcentaje = 100+newBpx/$(document).width()*100;
                newBpx = porcentaje+"%";
            }
        }
        elem.css('backgroundPosition', newBpx + ' ' + bpy );
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";expires=" + expires +";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }return "";
}

// CoachMarks
$(document).ready(function(){
    function showCoach(coachType, cookieValue)
    {
        $('.coach').show();
        setCookie("coachCookie", cookieValue + "|" + coachType, 720);
    }
    if($(".close-coach").length>0
        && location.pathname.search(/^(\/wps\/portal)?\/(personas|empresas|acerca\-de|banca\-inversion|fiduciaria|leasing\-bancolombia|renting\-colombia|valores\-bancolombia|factoring|asset\-management|valorespanama|fiduperu|leasing\-peru|sucursal\-panama|panama|cayman|bancolombia\-puerto\-rico)(\/)?$/i)>=0
    ){
        $(".top-nav .derecha.filiales").css("display","block");
        $(".top-nav .derecha.filiales a").click(function(){
            $('.coach').show(); return false;
        });

        $(".close-coach").click(function(){
            $('.coach').hide();
        });
        $(".coach").click(function(){
            $('.coach').hide();
        });

        var coachCookie = getCookie("coachCookie");
        var coachType = $(".coach-img").attr("class").split(" ")[1];

        if(coachCookie)
        {
            if(coachCookie.indexOf(coachType)<0)
                showCoach(coachType, coachCookie);
        }
        else
            showCoach(coachType, "");
    }
});


// Alto entradilla grande
function calcularAlto() {
    jQuery('.container-fluid.entradilla-grande-wizard:nth-child(odd)').each(function () {
        if (jQuery(this).is(':visible')) {
            jQuery(this).height(jQuery(this).height());
        }
    });
}
calcularAlto();

//tarjetas de credito home
$(document).ready(function () {

    $(".productos .destacados-categoria").on('click', function () {

        $('html, body').animate({
            scrollTop: $(".tab-content").offset().top
        }, 1000);

        if (!($(this).hasClass("active"))) {
            $(".productos .destacados-categoria").removeClass('active');
            $(this).addClass('active');
        }
    });

});

jQuery('#resumen-height').click(function () {
  autoHeightObjt('.col-md-4 .entradilla-con-imagenes .contenido');
});

//preference home
var cookieHomePreferenceNm = "preferenciahome";

function loadHomePreference(){
    var urlActual = window.location.pathname;
    if(urlActual.match(/^(\/wps\/portal)?\/(personas|empresas)\/\!ut\//i)) urlActual = urlActual.split("!")[0];
    if(urlActual.slice(-1)!="/") urlActual = urlActual + "/";

    if(urlActual.match(/^(\/wps\/portal)?\/(personas|empresas)(\/)?$/i))
    {
        var urlReferrer = document.referrer;

        if(urlReferrer == null || urlReferrer == "")
            validateHomePreference(urlActual);
        else if(urlReferrer.match(/www\.grupobancolombia\.com(\/wps)?(\/portal)?(\/)?$/i))
            validateHomePreference(urlActual);
        else
            setCookie(cookieHomePreferenceNm, urlActual, 365);
    }
}

function validateHomePreference(urlActual)
{
    var valorHomePreferencia =  getCookie(cookieHomePreferenceNm);

    if(valorHomePreferencia != null && valorHomePreferencia != "" && valorHomePreferencia != urlActual)
        window.location = getCookie(cookieHomePreferenceNm);
    else
        setCookie(cookieHomePreferenceNm, urlActual, 365);
}

loadHomePreference();

// Imagen click componente resumen - destacado
$( document ).ready(function() {
    $('.img-click').on('click', function() {
        var enlace1 = $(this).parent().find('.leer-mas a').attr('href');
        var enlace2 = $(this).parent().find('.link a').attr('href');
        if (enlace1 != "#" && enlace1 != "" && enlace1 != undefined){
            window.location.href = enlace1;
        }
        else if (enlace2 != "#" && enlace2 != "" && enlace2 != undefined){
            window.location.href = enlace2;
        }
    });
});


/* Tabs sucursales */
$(document).ready(function() {
    $('.sucursales-tabs ul.nav-tabs > li > a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
})

/* Filtro sucursales */
$(document).ready(function() {
    $( ".bt-close-filter" ).click(function() {
        $(".bt-close-filter").toggleClass("filter-down");
        $(".filtros-sucursales").toggle();
    });
});


// Tooltips
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});







//Modal
// $(document).ready(function() {
    $('.modal-launcher').click(function() {
        if ($(this).hasClass("img-to-video")){
            var enlace = $(this).attr('data-youtube')
            $('.modal-body').html('<div class="embed-responsive embed-responsive-16by9"><iframe width="568" height="314" src="'+enlace+'"" frameborder="0" allowfullscreen></iframe></div>');
        }
        else if ($(this).parent().hasClass("contenedor-mvideo")) {
            var enlace =  $(this).siblings('.m-video').attr('data-youtube');
            $('.modal-body').html('<div class="embed-responsive embed-responsive-16by9"><iframe width="568" height="314" src="'+enlace+'"" frameborder="0" allowfullscreen></iframe></div>');
        }
        else if ($(this).parent().hasClass("contenedor-mparrafo")){
            parrafo = $(this).siblings('.p-modal')[0].outerHTML

            $('.modal-body').html(parrafo);
            $('.modal-body .p-modal').toggleClass('oculto')
        }
        else if ($(this).parent().hasClass("contenedor-mimagen")){
            parrafo = $(this).siblings('.img-modal')[0].outerHTML

            $('.modal-body').html(parrafo);
            $('.modal-body .img-modal').toggleClass('oculto')
        }
        /*else if ($(this).parent().hasClass("contenedor-mimagen")){
            var imgsrc = $(this).attr('src');
            $('.modal-body').html('<img class="img-responsive" src="'+imgsrc+'" img>');
        };*/
    });
// });

    $('.modal-dialog .close').click(function(){
        $('iframe').attr('src', $('iframe').attr('src'));
    });


//Modal 2
$('#modal-prueba').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});

//Transacciones - Mobile Detect
function MM_openBrWindow(theURL,winName,features){//v2.0
    window.open(theURL,winName,features);
    }
var md = new MobileDetect(window.navigator.userAgent);
var dispositivo;
    if (md.tablet() != null) {
      dispositivo = "tablet";
    }else if (md.phone() != null) {
      dispositivo = "phone";
    }else if (md.mobile() != null) {
      dispositivo ="mobile";
    }else{
      dispositivo ="desktop";
    };

if (document.getElementById("transacciones-canales")) {
    var element = document.getElementById("transacciones-canales");
    element.classList.add(dispositivo);
}

//Oficinas Empresariales
$(document).ready(function() {
    $('.selector select').change(function() {
        $('.contentSelect').hide();
        $('.' + $(this).val()).show();
    });
});

// Compartir ofertas
$(document).on("click", ".btn-share", function() {
    if ($(this).next().hasClass('active-share')) {
        $('.active-share').toggle("slow");
        $(this).next().removeClass('active-share');

    }
    else {
        $('.cont-offers .share').each(function(index) {
            $('.active-share').toggle("slow");
            $('.network-share').removeClass('active-share');
        });
        $(this).next().addClass('active-share');
        $('.active-share').toggle("slow");
    }
});



// Datepicker Filtro historico
  //$(function() {
    //$( "#datepicker" ).datepicker();
  //});


// Scroll inactivo
$(window).load(function() {
  $('.slide-content').attr("id","cuerpo");
});

   $(function() {

    // Toggle Nav on Click
    $('.offer-filter').click(function() {
        // Calling a function in case you want to expand upon this.
        $('#cuerpo').addClass('show-nav');
        $('#cuerpo').addClass('quitar');
        $('body').css('overflow-y','hidden');
    });

    $('.filter-close, .btn-apply').click(function() {
        $('#cuerpo').removeClass('show-nav');
        $('#cuerpo').removeClass('quitar');
        $('body').css('overflow-y','inherit');
    });
});

// Filtro item seleccionado
$(document).on("click", ".filter-items li", function() {
  $this = $(this);
  if ($this.hasClass('select-item')) {
      $this.removeClass('select-item');
  }
  else {
    $this.siblings().removeClass('select-item');
    $this.addClass('select-item');
  }
});

$(document).on("click", ".btn-clear", function() {
    if ($('.filter-items li').hasClass('select-item')) {
        $('.filter-items li').removeClass('select-item');
    }
});

$(document).ready(function() {

    function height_filter(){
        var windowHeight = jQuery(window).height() - 120;
        jQuery('.filter-menu .row-fluid.menu').css({
            height:windowHeight+'px'
        });
        jQuery('.filter-menu .tab-content').css({
            height:windowHeight+'px'
        });
    }

    jQuery(function(){
        height_filter();
        jQuery(window).resize(function(){
            height_filter();
        });
    });

});

/* Sala de Prensa - mover filtros */
$(document).ready(function(){
    if ($(window).width() <= 767) {
        jQuery('.filtro-historico').prependTo('.all-article .resultados-de-busqueda');
    }
});


/* Tabs modal responsive */
$(document).ready(function(){
    if ($(window).width() <= 980) {
        jQuery('.tabs-verticales.tabs-horizontal .modal').prependTo('#main-content');
    }
});




/* Giros */
equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.giros .col-sm-3');
  equalheight('.paso-a-paso-aprende .col-xs-12');
});


$(window).resize(function(){
  equalheight('.giros .col-sm-3');
  equalheight('.paso-a-paso-aprende .col-xs-12');
});

$(document).ready(function() {
    $(".giros .col-sm-3").each(function(){
        if ($(this).is(':hidden')){
            $(this).remove();
        }
    });
});

// Imágenes animadas - Infográfico
$(document).ready(function() {
    new WOW().init();
});
