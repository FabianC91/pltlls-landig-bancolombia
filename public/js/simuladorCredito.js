$(document).ready(function(){
    console.log('script');
    $(window).resize(function(){
        if($(window).width() < 550) {
          $('.slick-simuladores-sfc').slick({
                  dots: false,
                  prevArrow: false,
                  nextArrow: false,
                  infinite: true,
                  slidesToShow: 2,
                  slidesToScroll: 4,
                  responsive: [
                                {
                                  breakpoint: 550,
                                  settings: {
                                    dots: false,
                                    prevArrow: false,
                                    nextArrow: false,
                                    infinite: true,
                                      slidesToShow: 2,
                                      slidesToScroll: 2
                                     
                                  }
                                  },
                                  {
                                  breakpoint: 480,
                                  settings: {
                                    dots: false,
                                    prevArrow: false,
                                    nextArrow: false,
                                    infinite: true,
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                     
                                  }
                                  }
                              ]
          });
               } else {
           
               }
          });

});