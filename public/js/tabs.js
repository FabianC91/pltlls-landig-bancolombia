/* NavegaciÃƒÂ³n Vertical */

$(document).ready(function() {
    $('.tabs-verticales-agro .nav-tabs > li > a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    responsiveTabsAgro.init();
})

var responsiveTabsAgro = ( function ( $, responsiveTabsAgro ) {

    responsiveTabsAgro.init = function () {

        responsiveTabsAgro.currentPosition = 'tabs';

        var tabGroups = $( ' .nav-tabs.responsive-agro' );
        var hidden    = '';
        var visible   = '';
        var activeTab = '';

        collapseDisplayed = [ 'xs', 'sm' ];

        $.each( collapseDisplayed, function () {
            hidden  += ' hidden-' + this;
            visible += ' visible-' + this;
        } );

        $.each(tabGroups, function () {
            var $tabGroup   = $( this );
            var tabs        = $tabGroup.find( 'li a' );
            var collapseDiv = $( '<div></div>', {
                'class' : 'acordeon-agro ' + visible,
                'id'    : 'collapse-' + (parseInt(Math.random()*100))
            } );

            $.each( tabs, function () {
                var $this          = $( this );
                var oldLinkClass   = $this.attr( 'class' ) === undefined ? '' : $this.attr( 'class' );
                var newLinkClass   = 'accordion-toggle';
                var oldParentClass = $this.parent().attr( 'class' ) === undefined ? '' : $this.parent().attr( 'class' );
                var newParentClass = 'panel panel-default';
                var newHash        = $this.get( 0 ).hash.replace( '#', 'collapse-' );

                if ( oldLinkClass.length > 0 ) {
                    newLinkClass += ' ' + oldLinkClass;
                }

                if ( oldParentClass.length > 0 ) {
                    oldParentClass = oldParentClass.replace( /\bactive\b/g, '' );
                    newParentClass += ' ' + oldParentClass;
                    newParentClass = newParentClass.replace( /\s{2,}/g, ' ' );
                    newParentClass = newParentClass.replace( /^\s+|\s+$/g, '' );
                }

                if ( $this.parent().hasClass( 'active' ) ) {
                    activeTab = '#' + newHash;
                }
                var titulo = '<h3 class="titulo_acordeon ' + newHash + '">'+$this.html()+'<li class="pull-right glyphicon glyphicon-chevron-down"></li></h3>'
                collapseDiv.append(titulo);
                collapseDiv.append('<div class="pane"><div class="row"><div id="'+newHash+'" class="col-xs-12">'+$($(this).parent().find('a').attr('href')).html()+'</div></div></div>');
            } );

            $tabGroup.next().after( collapseDiv );
            $tabGroup.addClass( hidden );
            $( '.tab-content.responsive' ).addClass( hidden );
        } );

        if ( activeTab ) {
            $( activeTab ).collapse( 'show' );
        }
    };

    return responsiveTabsAgro;
}( window.jQuery, responsiveTabsAgro || { } ) );

/*Acordeon*/
$(document).ready(function() {
    $(".tabs-verticales-agro .acordeon-agro > h3.titulo_acordeon").click(function() {
        clearCurrentAccordion($(this).parent());
        var alturaTitulo = $(this).outerHeight();

        if (!$(this).hasClass("current") && $(this).next().queue().length === 0) {
            $.when($(this).next().slideDown()).then(function(){
                $("html, body").animate({scrollTop: $(this).offset().top - alturaTitulo }, 400);
            });
            $(this).addClass("current");
            $(this).find('li').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $(this).find('span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    });

    function clearCurrentAccordion(accordion)
    {
        $(accordion).find("h3.titulo_acordeon").each(function(i, item){
            $(this).next().slideUp();
            $(item).removeClass("current");
            $(this).find('li').addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
            $(this).find('span').addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
        });
    }

    /* Habilitando el primero por defecto
    $('.acordeon').each(function() {
        $($(this).find('h3.titulo_acordeon')[0]).trigger('click');
    });*/
});



$(document).ready(function() {

    //Ocultar breadcrumb en el home
    //if(location.pathname.search(/^(\/wps\/portal)?\/(personas|empresas)(\/)?$/i)>=0 || location.pathname.search(/^(\/wps\/portal)?\/(personas|empresas)\/\!ut\//i) >=0) $(".breadcrumbs").hide();

    function autoHeightObjt(select){
        if($(window).width()>=768)//Is a desktop
        {

            //$(".tabs-verticales-agro .tab-content > div").addClass("active");    // Mostrar los tab-pane para calcular el alto de los elementos
            var objHeight=[];
            var maxHeight = 0;

            $(select).each(function(i, item){
                objHeight.push($(item));
                if(maxHeight<objHeight[i].height())
                    maxHeight = objHeight[i].height();
            });
            for (i = 0; i < objHeight.length; ++i) {
                objHeight[i].height(maxHeight);
            }
            //$(".tabs-verticales-agro .tab-content > div").removeClass("active"); // Quitar el active de los tab-pane para que se oculten
            //$(".tabs-verticales-agro .tab-content > div:first").addClass("active"); // Mostrar el primero
        }
    }
    if($(".tabs-verticales-agro").length) $(".tabs-horizontal .tab-content").css("min-height", $(".tabs-verticales-agro #filialTabs").height());
    if($(".tabs-verticales-agro").length && $(".tabs-verticales-agro .nav-tabs").length) $(".tabs-horizontal .tab-content").css("min-height", $(".tabs-verticales-agro .nav-tabs").height());

});
